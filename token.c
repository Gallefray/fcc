#include "header.h"

char tok[TOKSIZE];
long int ccnt = 0;
long int lcnt = 0;

itype gettok(char *t)
{
	char inc;
	itype ity = NONE;
	t[0] = 0; // for stccat;
	while (1) {
		inc = getc(stdin);
		if (ity == NONE) 
		{
			if (inc == '\"') {
				ity = STRN;
				stccat(t, inc);
				ccnt++;
			}
			else if (inc == '\'') {
				ity = CHAR;
				stccat(t, inc);
				ccnt++;
			}
			else if (isdigit(inc)) {
				ity = NUM;
				stccat(t, inc);
				ccnt++;
			}
			else if (islower(inc) || isupper(inc)) {
				ity = IDEN;
				stccat(t, inc);
				ccnt++;
			}
			else if (iswspac(inc))
				whitspac(inc);
			else if (inc == EOF) {
				ity = DONE;
				break;
			}
		}
		else 
		{
			if (ity == NUM) 
			{
				if (isdigit(inc)) {
					stccat(t, inc);
					ccnt++;
				} 
				else if (islower(inc) || isupper(inc)) {
					if (inc == 'l' || inc == 'L' || inc == 'u' ||
						inc == 'U') 
					{
						stccat(t, inc);
						ccnt++;
						break;
					}
					else {
						err("Expected NUM, \'l\' or \'u\', got \'%c\'", "\0",
							inc);
					}
				}
				else {
					ungetc(inc, stdin);
					break;
				}
			}
			else if (ity == IDEN) {
				if (isdigit(inc) || islower(inc) || isupper(inc)) {	
					stccat(t, inc);
					ccnt++;
				}
				else {
					ungetc(inc, stdin);
					break;
				}
			}
			else if (ity == STRN) {
				stccat(t, inc);
				ccnt++;
				if (inc == '\"')
					break;
			}
			else if (ity == CHAR) {
				stccat(t, inc);
				ccnt++;
				if (inc == '\'')
					break;
			}
		}
	}
	return ity;
}

char getchr()
{
	char inc;
	while (1) {
		inc = getc(stdin);
		if (iswspac(inc))
			whitspac(inc);
		else {
			ccnt++;
			return inc;
		}
	}
}

bool iswspac(char c)
{
	if (c == '\n' || c == ' ' || c == '\t')
		return true;
	
	return false;
}

void whitspac(char c)
{
	if (c == '\n') {
		lcnt++;
		ccnt ^= ccnt;
	} 
	else if (c == ' ' || c == '\t') {
		ccnt++;
	}
}
