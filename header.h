#ifndef FCC_HEADER
#define FCC_HEADER
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdarg.h>

extern const char *progn;
extern char *infile;
extern FILE *outfile; 
#define TOKSIZE 128

typedef enum {false, true} bool;
typedef enum {
    NONE,
	DONE,
    NUM,
	IDEN,
	STRN,
	CHAR,
	OPER
} itype; 

typedef struct {
	char scope[TOKSIZE];
	char var[TOKSIZE];
	void *p, *n;
} astrec;

astrec* astpt;

extern char tok[TOKSIZE];
extern long int ccnt, lcnt;

// main.c

// token.c
itype gettok(char t[]);
char getchr(void);
bool iswspac(char c);
void whitspac(char c);

// parse.c
void parse(void);
bool match(char *s);

bool stat(char *s);
bool contflow(char *s);
bool func(char *s);

bool type(void);
bool num(void);
bool ident(void);
bool op(void);
bool string(void);

void expect(char c);
bool ischar(char c);
bool istype(void);
bool isnum(void);
bool isident(void);
bool isop(void);
bool isstring(void);
bool isrkey(char *s);

bool chktype(void);
bool chknum(void);
bool chkident(void);
bool chkstring(void);

// ast.c
void astload(char *scope, char *var);
int astget(char *var);
void astdel(char *scope, char *var);

// emit.c
void emit(char *s, ...);

// util.c
void stccat(char *s, char c);
void ungetstr(char s[]);
bool matstr(const char *s);
void err(const char *s, const char *t, ...);
#endif
