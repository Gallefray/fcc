#include "header.h"

itype ity = NONE;

char *rkey[3] = {
	"asm",
	"char",
	"int"
};
int rkey_n = 4;

void parse()
{
	while (ity != DONE) {
		ity = gettok(&tok[0]);
		if (ity == IDEN) {
			match(tok) ? func(tok) : err(":: debug ::", tok);
		}
	}
}

bool match(char *s)
{
	(!stat(s)) ? (!contflow(s)) : false;
}

bool stat(char *s)
{
	if (matstr("asm")) {
		isstring();
		expect(';'); 
		char *t = &s[1]; // remove the first "
		char *out = malloc(strlen(s)-2);
		strncpy(out, t, strlen(s)-2); // and the second "
		emit(out);
		return true;
	}
	else if (chktype()) {
		// Temporary -- needs cleaning
		char typ[3] = {0};
		if (matstr("int"))
			strcat(typ, "dw");
		else if (matstr("char"))
			strcat(typ, "db");
		else
			err("Unrecognized type", tok);
	
		isident();
		if (ischar(';')) {
			emit("%s: %s 0", s, typ);
			return true;
		}
		if (ischar('=')) {
			char *id = malloc(strlen(s));
			id[0] = 0;
			strcat(id, s);
			if (isnum()) {
				num();
				expect(';');
				emit("%s: %s %s", id, typ, s);
			}
			free(id);
			return true;
		}  
		//if (ischar('('))
		// err("Expected ';', '=' or function definition.'", "\0");
	}
	else if (ident()) {
		if (ischar('(')) {
			expect(')');
			expect(';');
			emit("call %s", s);
			return true;
		}
		if (ischar('=')) {
			char *id = malloc(strlen(s));
			*id = 0;
			strcat(id, s);
			if (isnum()) { // replace with expr
				num();
				emit("mov [%s], %s", id, s);
				return true;
			}
		}
	}
	return false;
}

bool contflow(char *s) {
	return false;
}

bool func(char *s)
{
	itype savity = ity;
	if (chkident()) {
		expect('(');
		// TO_DO: insert argdef/expr/whatever();
		expect(')');
		if (ischar(';')) {
			emit("extern %s", s);
			return true;
		}
		if (ischar('{')) {
			emit("");
			emit("%s:", s);
			emit("    push bp");
			emit("    mov bp, sp");
			// TO_DO: Get args working.
			while (!ischar('}')) {
				ity = gettok(&tok[0]);
				if (ity == IDEN) {
					printf("    ");
					match(tok);
				}
			}
			ity = savity;
			emit("    mov sp, bp");
			emit("    pop bp");
			emit("ret", s);
			emit("");
			return true;
		}
	}
	return false;
}

void expr(char *s)
{
	
}

bool type()
{
	if (!(matstr("char") || matstr("int")))
		err("Expected type, got `%s`", "\0", tok);

	return true;
}

bool num()
{
	if (ity != NUM)
		err("Expected number, got `%s`", "\0", tok);

	return true;
}

bool ident()
{
	if (ity != IDEN && isrkey(tok))
		err("Expected identifier, got `%s`", "\0", tok);

	return true;
}

bool op()
{
	char inc = getchr();
	if (!(inc == '+' || inc == '-' || inc == '\\' || inc == '*'))
		err("Unrecognized character `%c`, expected operator", "\0", inc);

	return true;
}

bool string()
{
	if (ity != STRN)
		err("Expected token of type string", "\0");

	return true;
}

void expect(char c)
{
	char inc;
	inc = getchr();
	ccnt++;
	if (inc == EOF) 
		err("Expected \'%c\', got EOF", "\0", c);
	else if (inc != c) {
		err("Expected \'%c\', got \'%c\'", "\0", c, inc);
	}
}

bool ischar(char c)
{
	char inc;
	inc = getchr();
	if (inc == EOF) {
		err("Expected \'%c\', got EOF", "\0", c);
	}
	else if (inc != c) {
		ungetc(inc, stdin);
		return false;
	}
	else if (inc == c) {
		return true;
	}
}

bool istype()
{
	ity = gettok(&tok[0]);
	if (matstr("char") || matstr("int"))
		return true;
	else
		return false;
}

bool isnum()
{
	ity = gettok(&tok[0]);
	if (ity == NUM)
		return true;

	return false;
}

bool isident()
{
	ity = gettok(&tok[0]);
	if (ity != IDEN && isrkey(tok))
		return false;

	return true;
}

bool isop()
{
	char inc = getchr();
	if (inc == '+' || inc == '-' || inc == '\\' || inc == '*')
		return true;
}

bool isstring()
{
	ity = gettok(&tok[0]);
	if (ity != STRN)
		return false;

	return true;
}

bool isrkey(char *s)
{
	int i;
	for (i = 0; i < rkey_n; i++) {
		if (strcmp(s, rkey[i]) == 0)
			return true;
	}
	return false;
}

bool chktype()
{
	if (matstr("char") || matstr("int"))
		return true;
	else
		return false;
}

bool chknum()
{
	if (ity == NUM)
		return true;

	return false;
}

bool chkident()
{
	if (ity != IDEN && isrkey(tok))
		return false;

	return true;
}

bool chkstring()
{
	if (ity != STRN)
		return false;

	return true;
}

