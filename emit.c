#include "header.h"

void emit(char *s, ...)
{
	va_list a;
	va_start(a, s);
	char buf[256] = {0}; 

	int i;
	for (i = 0; i < 256; i++) {
		if (s[i] == 0) {
			break;
		}
		else if (s[i] == '%') {
			i++;
			if (s[i] == 's') {
				strcat(buf, va_arg(a, char *));
			}
		}
		else {
			stccat(buf, s[i]);
		}
	}
	va_end(a);
	printf("%s\n", buf); // Something more complex need go here
}
