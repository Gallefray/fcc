# -g3 loads the symbols into the file (for debugging)
build:
	gcc main.c token.c parse.c ast.c emit.c util.c -o fcc

t:
	cat test.c | ./fcc

db:
	gdb fcc

bt: 
	gcc -g3 main.c token.c parse.c ast.c emit.c util.c -o fcc
	cat test.c | ./fcc
