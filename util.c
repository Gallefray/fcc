#include "header.h"
void stccat(char *s, char c)
{
    char tmp[2] = {' ', '\0'};
    tmp[0] = c;
    strcat(s, tmp);
}

void ungetstr(char s[])
{
    int i;
    for (i = 0; i < strlen(s); i++) {
		//if (s[i] != 0)
		ungetc(s[i], stdin);
    }
}

bool matstr(const char *s)
{
    if (strcmp(s, tok) == 0) {
		return true;
    } else 
		return false;
}

void err(const char *s, const char *t, ...)
{
	va_list a;
	va_start(a, t);
	char buf[256] = {0};
	int i;
	for (i = 0; i < 256; i++) {
		if (s[i] == 0) {
			break;
		}
		else if (s[i] == '%') {
			i++;
			if (s[i] == 's') {
				strcat(buf, va_arg(a, char *));
			}
			else if (s[i] == 'c') {
				stccat(buf, (char)va_arg(a, int));
			}
		}
		else {
			stccat(buf, s[i]);
		}
	}
	va_end(a);
	printf("\n");
    printf("%s error: %s at line %ld, column %ld.\n", progn, buf, ++lcnt, --ccnt);
	printf("Offending token(if any): `%s`\n", t);
    printf("%s terminating.\n", progn);
    exit(-1);
}
